¿Qué es :nth-of-type?: 

[Link a developer mozilla](https://developer.mozilla.org/en-US/docs/Web/CSS/:nth-of-type).

No confundir con [:nth-child](https://developer.mozilla.org/en-US/docs/Web/CSS/:nth-child).

Se pueden seleccionar elementos en el mismo contenedor tal y como se presenta en el caso práctico con `:nth-child()`, pero si hay hijos/hermanos diferentes `:nth-of-type()` permite elegir más exactamente lo que se precisa.

Y como hemos utilizado [el modelo de color HSL](https://es.wikipedia.org/wiki/Modelo_de_color_HSL), una pequeña introducción al tema. Y [esta herramienta online](https://hslpicker.com) para elegir color no viene mal.